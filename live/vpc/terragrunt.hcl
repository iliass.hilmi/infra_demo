terraform {
  # Import module from repository
  source = "git::https://gitlab.com/iliass.hilmi/infra_demo.git//modules/vpc"
}

# Configure Terragrunt to automatically store tfstate files in an S3 bucket
# when the bucket not exist terragrunt will suggest to create new one
remote_state {
  backend = "s3"
  config  = {
    bucket         = "csa-bucket-project"
    key            = "terraform/stage/vpc/vpc.state"
    region         = "us-east-1"
  }
}

# Vpc inputs
inputs = {
  aws_region                = "us-east-1"
  vpc_name                  = "terraform-stage" 
  cidr_block                = "10.0.0.0/16"
  availability_zone         = ["us-east-1a","us-east-1b"]
  subnet_public_names       = ["terraform-stage-public-1","terraform-stage-public-2"]
  public_subnet_cidr        = ["10.0.1.0/24","10.0.2.0/24"]
  subnet_private_names      = ["terraform-stage-private-1","terraform-stage-private-1"]
  private_subnet_cidr       = ["10.0.4.0/24","10.0.5.0/24"]
  internet_gateway_name     = "terraform-stage-gateway"
  route_table_name          = "terraform-stage-route-table"
  route_table_cidr_block    = "0.0.0.0/0"
}






