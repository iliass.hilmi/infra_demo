# Internet VPC
resource "aws_vpc" "main" {
  cidr_block           = var.cidr_block
  instance_tenancy     = "default"
  enable_dns_support   = "true"
  enable_dns_hostnames = "true"
  enable_classiclink   = "false"
  tags = {
    Name = var.vpc_name
  }
}

# Subnets
resource "aws_subnet" "main_public_1" {
  vpc_id                  = aws_vpc.main.id
  cidr_block              = var.public_subnet_cidr[0]
  map_public_ip_on_launch = "true"
  availability_zone       = var.availability_zone[0]

  tags = {
    Name = var.subnet_public_names[0]
  }
}

resource "aws_subnet" "main_public_2" {
  vpc_id                  = aws_vpc.main.id
  cidr_block              = var.public_subnet_cidr[1]
  map_public_ip_on_launch = "true"
  availability_zone       = var.availability_zone[1]

  tags = {
    Name = var.subnet_public_names[1]
  }
}


resource "aws_subnet" "main_private_1" {
  vpc_id                  = aws_vpc.main.id
  cidr_block              = var.private_subnet_cidr[0]
  map_public_ip_on_launch = "false"
  availability_zone       = var.availability_zone[0]

  tags = {
    Name = var.subnet_private_names[0]
  }
}

resource "aws_subnet" "main_private_2" {
  vpc_id                  = aws_vpc.main.id
  cidr_block              = var.private_subnet_cidr[1]
  map_public_ip_on_launch = "false"
  availability_zone       = var.availability_zone[1]

  tags = {
    Name = var.subnet_private_names[1]
  }
}


# Internet GW
resource "aws_internet_gateway" "main_gw" {
  vpc_id = aws_vpc.main.id

  tags = {
    Name = var.internet_gateway_name
  }
}

# route tables
resource "aws_route_table" "main_public_1" {
  vpc_id = aws_vpc.main.id
  route {
    cidr_block = var.route_table_cidr_block
    gateway_id = aws_internet_gateway.main_gw.id
  }

  tags = {
    Name = var.route_table_name
    
  }
}

resource "aws_route_table" "main_public_2" {
  vpc_id = aws_vpc.main.id
  route {
    cidr_block = var.route_table_cidr_block
    gateway_id = aws_internet_gateway.main_gw.id
  }

  tags = {
    Name = var.route_table_name
    
  }
}

resource "aws_route_table" "main_private_1" {
  vpc_id = aws_vpc.main.id
  route {
    cidr_block = var.route_table_cidr_block
    gateway_id = aws_internet_gateway.main_gw.id
  }

  tags = {
    Name = var.route_table_name
  }
}

resource "aws_route_table" "main_private_2" {
  vpc_id = aws_vpc.main.id
  route {
    cidr_block = var.route_table_cidr_block
    gateway_id = aws_internet_gateway.main_gw.id
  }

  tags = {
    Name = var.route_table_name
  }
}
# route associations public
resource "aws_route_table_association" "main_public_1_a" {
  subnet_id      = aws_subnet.main_public_1.id
  route_table_id = aws_route_table.main_public_1.id
}

resource "aws_route_table_association" "main_public_2_a" {
  subnet_id      = aws_subnet.main_public_2.id
  route_table_id = aws_route_table.main_public_2.id
}

resource "aws_route_table_association" "main_private_1_a" {
  subnet_id      = aws_subnet.main_private_1.id
  route_table_id = aws_route_table.main_private_1.id
}

resource "aws_route_table_association" "main_private_2_a" {
  subnet_id      = aws_subnet.main_private_2.id
  route_table_id = aws_route_table.main_private_2.id
}




