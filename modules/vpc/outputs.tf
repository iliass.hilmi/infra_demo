output "public_subnets" {
  value = var.public_subnet_cidr
}

output "subnet_ids" {
  value = [aws_subnet.main_public_1.id, aws_subnet.main_public_2.id]
}

output "vpc_id" {
  value = aws_vpc.main.id
}

output "vpc_security_group_ids" {
  value = aws_vpc.main.default_security_group_id
}
     